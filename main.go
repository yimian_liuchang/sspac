//http://127.0.0.1:8090/proxy.pac -> http://127.0.0.1:1080/pac
package main

import (
	"io"
	"log"
	"net/http"
)

func main() {
	log.Fatal(http.ListenAndServe("127.0.0.1:8090",
		http.HandlerFunc(handle)))
}

func handle(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/proxy.pac" {
		http.NotFound(w, r)
		return
	}
	
	resp,err := http.Get("http://127.0.0.1:1080/pac")
	if resp != nil { defer resp.Body.Close() }
	if err != nil {
		http.Error(w, err.Error(),
			http.StatusInternalServerError)
		return
	}
	_,err = io.Copy(w, resp.Body)
	if err != nil {
		http.Error(w, err.Error(),
			http.StatusInternalServerError)
	}
}
